#!/bin/bash

python /django/manage.py collectstatic --noinput

# sleep .10
# python /django/manage.py migrate

python /django/set_webhook.py

uwsgi --ini /django/uwsgi.ini
