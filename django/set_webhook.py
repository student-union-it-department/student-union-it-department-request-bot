import requests
import sys
import os


if __name__ == "__main__":
    token = os.environ["TELEGRAM_BOT_TOKEN"]
    scheme = os.environ["SCHEME"]
    domain = os.environ["DOMAIN"]
    response = requests.get(f"https://api.telegram.org/bot{token}/setWebhook?url={scheme}://{domain}/telegram_bot/webhooks/ideabot/")
    response = response.json()

    if not response["ok"]:
        print("Error when setting webhook")
        print("Response: ", response)
        sys.exit(1)

    print("Setting webhook done.")
