from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from TelegramBot.views import TelegramBotView

urlpatterns = [
    path('webhooks/ideabot/', csrf_exempt(TelegramBotView.as_view())),
]