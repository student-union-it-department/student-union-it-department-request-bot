from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from django.conf import settings
from django.template.loader import render_to_string

import json
import requests
import logging

from TelegramBot.models import User, Idea


logger = logging.getLogger(__name__)


class TelegramBotView(View):
    def parse_request(self, data):
        if 'callback_query' in data:
            data = data['callback_query']
            from_user = data['from']
            chat_id = data['message']['chat']['id']
            message_id = data['message']['message_id']
            text = data['data']
            return True, message_id, from_user, chat_id, text

        message = data['message']
        from_user = message['from']
        chat_id = message['chat']['id']
        text = message.get('text', None)
        return False, -1,    from_user, chat_id, text
        

    def post(self, request, *args, **kwargs):        
        data = json.loads(request.body)

        if 'channel_post' in data:
            return JsonResponse({'ok': 'POST request processed'})

        is_callback, callback_message_id, from_user, chat_id, text = self.parse_request(data)
        if text is None:
            return JsonResponse({'ok': 'POST request processed'})

        if chat_id != settings.TELEGRAM_ADMIN_CHANNEL:
            if not User.objects.filter(telegram_id=from_user['id']).exists():
                telegram_user = User.objects.create(telegram_id=from_user['id'], first_name=from_user['first_name'],
                 last_name=from_user.get('last_name', ''), username=from_user['username'], expected_command=0)
                telegram_user.save()
            telegram_user = User.objects.get(telegram_id=from_user['id'])

            if text == '/start':
                self.send_greeting(chat_id)
            elif text == '/help':
                self.send_help(chat_id)
            elif text == '/my_ideas':
                if telegram_user.expected_command != 0:
                    self.send_form_question(12, chat_id)
                ideas = Idea.objects.filter(user=telegram_user)
                rendered_ideas = render_to_string('TelegramBot/ideas.html', {'ideas': ideas})
                if len(rendered_ideas.strip()) > 0:
                    self.send_message(rendered_ideas, chat_id)
                else:
                    self.send_form_question(14, chat_id) # Вы пока не отправляли идей
                telegram_user.delete_current_idea()
            elif text == '/new_idea':
                telegram_user.delete_current_idea()
                idea = Idea.objects.create(user=telegram_user)
                idea.save()
                telegram_user.set_proc_idea(idea)
                self.send_form_question(telegram_user.expected_command, chat_id) # self.send_message('Введите заголовок', chat_id) # 1
            elif text == '/stop':
                if telegram_user.expected_command == 0:
                    self.send_form_question(11, chat_id) # нет несохраненный идей 
                else:
                    telegram_user.delete_current_idea()
                    self.send_form_question(12, chat_id) # Заявка удалена
                    # self.send_form_question(telegram_user.expected_command, chat_id) # 'Все изменения удалятся. Вы уверены? (yes/no)'
            else:
                if telegram_user.expected_command == 0: 
                    self.send_form_question(0, chat_id) # invalid command # 0
                else:
                    idea = None
                    if Idea.objects.filter(id=telegram_user.proc_idea).exists():
                        idea = Idea.objects.get(id=telegram_user.proc_idea)

                    # entering title
                    if telegram_user.expected_command == 1:
                        if is_callback:
                            self.send_form_question(0, chat_id)
                            return JsonResponse({'ok': 'POST request processed'})

                        idea.title = text
                        idea.save()

                        telegram_user.set_expected_command(2)
                        self.send_form_question(telegram_user.expected_command, chat_id) # Введите описание
                    # entering description
                    elif telegram_user.expected_command == 2:
                        if is_callback:
                            self.send_form_question(0, chat_id)
                            return JsonResponse({'ok': 'POST request processed'})
                            
                        idea.description = text
                        idea.save()

                        telegram_user.set_expected_command(3)
                        self.send_form_question(telegram_user.expected_command, chat_id) # Будете ли вы принимать участие в разработке? (yes/no)
                    # entering particip
                    elif telegram_user.expected_command == 3:
                        text = text.lower()
                        if is_callback and text == 'yes1':
                            idea.participation = True
                            telegram_user.set_expected_command(4)
                            self.resend_message(chat_id, callback_message_id, 'You will take part in the development ✅')
                            self.send_form_question(telegram_user.expected_command, chat_id) # 'У тебя есть команда? (yes/no)'
                        elif is_callback and text == 'no1':
                            idea.participation = False
                            idea.team = False
                            idea.status = 1
                            telegram_user.set_expected_command(0)
                            self.resend_message(chat_id, callback_message_id, "You won't not take part in the development ❌")
                            self.send_form_question(5, chat_id) # спасибо за идею
                            self.send_idea(telegram_user, idea)
                        else:
                            self.send_form_question(13, chat_id) # Вы можете ответить только yes/no   
                            return JsonResponse({'ok': 'POST request processed'})
                        idea.save()
                    # entering team
                    elif telegram_user.expected_command == 4:
                        text = text.lower()
                        if is_callback and text == 'yes2':
                            idea.team = True
                            self.resend_message(chat_id, callback_message_id, 'You have a team ✅')
                        elif is_callback and text == 'no2':
                            idea.team = False
                            self.resend_message(chat_id, callback_message_id, "You don't have a team ❌")
                        else:
                            self.send_form_question(13, chat_id) # Вы можете ответить только yes/no 
                            return JsonResponse({'ok': 'POST request processed'})

                        idea.status = 1
                        self.send_form_question(5, chat_id) # спасибо за идею
                        telegram_user.set_expected_command(0)
                        idea.save()
                        self.send_idea(telegram_user, idea)
                
                        
        return JsonResponse({'ok': 'POST request processed'})


    def send_greeting(self, chat_id):
        rendered = render_to_string('TelegramBot/greeting.html')
        self.send_message(rendered, chat_id)
        self.send_help(chat_id)


    def send_help(self, chat_id):
        rendered = render_to_string('TelegramBot/help.html')
        keyboards = json.loads(open('TelegramBot/jsons/help_buttons.json').read())
        self.send_message(rendered, chat_id, keyboards=keyboards['keyboards'])


    def send_idea(self, user, idea):
        rendered = render_to_string('TelegramBot/report.html', {
            'user': user,
            'idea': idea,         
            'domain': settings.DOMAIN,
            'scheme': settings.SCHEME,
        })
        self.send_message(rendered, settings.TELEGRAM_ADMIN_CHANNEL)

    
    def send_form_question(self, command_code, chat_id):
        form_questions = json.loads(open('TelegramBot/jsons/form_questions.json').read())
        response = form_questions[str(command_code)]
        if 'keyboards' in response:
            self.send_message(response['text'], chat_id, keyboards=response['keyboards'])
        else:
            self.send_message(response['text'], chat_id)


    @staticmethod
    def edit_message_markup(chat_id, message_id, reply_markup):
        data = {
            'chat_id': chat_id,
            'message_id': message_id,
            'reply_markup': reply_markup,
        }
        response = requests.post(
            f'{settings.TELEGRAM_URL}{settings.TELEGRAM_BOT_TOKEN}/editMessageReplyMarkup', data=data
        )
        response_json = response.json()
        if not response_json['ok']:
            logger.error(f'MessageMarkup not edited: {response_json}')

    @staticmethod 
    def delete_message(chat_id, message_id):
        data = {
            'chat_id': chat_id,
            'message_id': message_id,
        }
        response = requests.post(
            f'{settings.TELEGRAM_URL}{settings.TELEGRAM_BOT_TOKEN}/deleteMessage', data=data
        )
        response_json = response.json()
        if not response_json['ok']:
            logger.error(f'Message not deleted: {response_json}')

    @staticmethod
    def send_message(message, chat_id, parse_mode='HTML', keyboards=None):
        data = {
            'chat_id': chat_id,
            'text': message,
            'parse_mode': parse_mode,
        }
        if keyboards:
            data['reply_markup'] = json.dumps({'inline_keyboard': keyboards})


        response = requests.post(
            f'{settings.TELEGRAM_URL}{settings.TELEGRAM_BOT_TOKEN}/sendMessage', data=data
        )
        response_json = response.json()
        if not response_json['ok']:
            logger.error(f'Message not sent: {response_json}')


    @staticmethod
    def send_new_status(idea):
        message = render_to_string('TelegramBot/new_status_notification.html', {'idea': idea, 'status': settings.IDEA_CATEGORY_MAPPING_INV[idea.status]})

        data = {
            'chat_id': idea.user.telegram_id,
            'text': message,
            'parse_mode': 'HTML'
        }
        response = requests.post(
            f'{settings.TELEGRAM_URL}{settings.TELEGRAM_BOT_TOKEN}/sendMessage', data=data
        )
        response_json = response.json()
        if not response_json['ok']:
            logger.error(f'Message not sent: {response_json}')


    
    def resend_message(self, chat_id, message_id, new_text):
        self.delete_message(chat_id, message_id)
        self.send_message(new_text, chat_id)
