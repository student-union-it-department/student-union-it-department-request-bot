from django.db import models


class User(models.Model):
    telegram_id =  models.IntegerField()
    
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    username = models.CharField(max_length=128)

    previous_expexted_command = models.IntegerField(default=0)
    expected_command = models.IntegerField(default=0)
    proc_idea = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.first_name} {self.last_name} ({self.username})'

    def delete_current_idea(self):
        if Idea.objects.filter(id=self.proc_idea).exists():
            Idea.objects.get(id=self.proc_idea).delete()
        self.proc_idea = 0
        self.previous_expexted_command = 0
        self.expected_command = 0
        self.save()

    def set_expected_command(self, code):
        if code == 0:
            self.proc_idea = 0
        self.previous_expexted_command = self.expected_command
        self.expected_command = code
        self.save()

    def backup_expected_command(self):
        self.expected_command = self.previous_expexted_command
        self.previous_expexted_command = 0
        self.save()

    def set_proc_idea(self, idea):
        self.proc_idea = idea.id
        self.expected_command = 1
        self.save()


class Idea(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=256)
    description = models.TextField()
    participation = models.BooleanField(default=False)
    team = models.BooleanField(default=False)
    
    status = models.IntegerField(default=0)
    comments = models.TextField()

    def __str__(self):
        if self.title == "":
            return f'Empty idea ({self.user.first_name} {self.user.last_name})'
        return f'{self.title} ({self.user.first_name} {self.user.last_name})'
