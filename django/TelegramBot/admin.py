from django.contrib import admin
from TelegramBot.models import User, Idea


admin.site.register(User)
admin.site.register(Idea)
