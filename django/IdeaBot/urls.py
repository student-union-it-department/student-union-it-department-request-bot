from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.http import HttpResponseRedirect


def web_redirect(request):
    return HttpResponseRedirect('/web/')


urlpatterns = [
    path('', web_redirect),
    path('admin/', admin.site.urls),
    path('telegram_bot/', include('TelegramBot.urls')),
    path('web/', include('WebInterface.urls')),
]
