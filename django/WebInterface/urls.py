from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from WebInterface import views


urlpatterns = [
    path('', views.redirect),
    path('auth/', views.auth),
    path('logout/', views.logout),
    path('panel/<str:category>/', views.panel),
    path('updateIdea/', views.update_idea),
    path('idea/<int:idea_id>/', views.edit_idea),
]
