from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.conf import settings
from django.views import View
from WebInterface.models import User
from TelegramBot.models import Idea
from TelegramBot.views import TelegramBotView


def redirect(request):
    if "id" in request.session:
        return HttpResponseRedirect('/web/panel/new')
    return HttpResponseRedirect('/web/auth')


def logout(request):
    if "id" in request.session:
        del request.session['id']
    return HttpResponseRedirect('/web/auth')


def auth(request):
    if 'id' in request.session:
        return HttpResponseRedirect('/web/panel/new')

    if request.method == 'GET':
        return render(request, 'WebInterface/auth.html', {
            'domain': settings.DOMAIN,
            'scheme': settings.SCHEME,
        })
    
    if 'login' not in request.POST or 'password' not in request.POST:
        return JsonResponse({'ok': False, 'error': 'Invalid request', 'error_code': 444})

    login = request.POST['login']
    password = request.POST['password']

    user = User.objects.filter(login=login)
    if user.count() == 1:
        user = user[0]
        if user.password == password:
            request.session['id'] = user.id
            return JsonResponse({'ok': True})
    
    return JsonResponse({'ok': False, 'error_code': 503}) 


def update_idea(request):
    if 'id' not in request.session:
        return JsonResponse({'ok': 'False', 'error_code': 403})

    if 'idea_id' not in request.POST or 'comment' not in request.POST or 'status' not in request.POST:
        return JsonResponse({'ok': False, 'error': 'Invalid request'})

    idea_id = request.POST['idea_id']
    comment = request.POST['comment']
    status = request.POST['status']

    try:
        idea_id = int(idea_id)
        status = int(status)
    except Exception:
        return JsonResponse({'ok': False, 'error': 'Invalid integer value'})

    if not Idea.objects.filter(id=idea_id).exists():
        return JsonResponse({'ok': False, 'error': 'This idea does not exists'})
    
    idea = Idea.objects.get(id=idea_id)
    idea.status = status
    idea.comments = comment
    idea.save()
    TelegramBotView.send_new_status(idea)
    return JsonResponse({'ok': True})


def panel(request, category):
    if 'id' not in request.session:
        return HttpResponseRedirect("/web/auth")

    if category not in settings.IDEA_CATEGORY_MAPPING:
        return HttpResponse('404')

    category_code = settings.IDEA_CATEGORY_MAPPING[category]
    ideas = Idea.objects.filter(status=category_code)

    return render(request, 'WebInterface/panel.html', {
        'domain': settings.DOMAIN,
        'scheme': settings.SCHEME,
        'ideas': ideas,
        'category_code': category_code,
    })


def edit_idea(request, idea_id):
    if 'id' not in request.session:
        return HttpResponseRedirect("/web/auth")

    if not Idea.objects.filter(id=idea_id).exists():
        return JsonResponse({'ok': False, 'error': 'Idea does not exists'})
    
    idea = Idea.objects.get(id=idea_id)
    return render(request, 'WebInterface/idea.html', {
        'domain': settings.DOMAIN,
        'scheme': settings.SCHEME,
        'idea': idea,
    })